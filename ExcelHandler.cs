﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Net;

namespace DBImporter
{
    class HTTPFileTransmitter
    {
        public void Send(string address, string filePath)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.UploadFile(address, filePath);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    class ExcelHandler
    {
        protected const string KEY_STRING = @"차량위치";
        public const string TEMPLATE_FILEPATH = @"template.xlsx";
        public const string TEST_FILEPATH = @"output_.xlsx";
        protected const String strConnection = @"Data Source=127.0.0.1; Initial Catalog=DBImportTest; User Id=test; Password=test;";

        protected int dataReadStartPos = 0; // 0 means not found 
                                            //Output Order
                                            //A2
                                            //1/3 , 2/4, 3/5, 4/6, 5/7, 6/8, 7/9(10000으로 나눔), 8/11, 9/12(km떼야함), 10/13, 11/14, 12/15, 13/16, 14/17, 15/18, 16/19, 17/20, 18/21, 19/22, 20/23, 21/24, 22/25

        public bool ImportExcelToDB(string inputFilePath)
        {
            try
            {
                string excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0\"", inputFilePath);
                //Create Connection to Excel work book 
                using (OleDbConnection excelConnection = new OleDbConnection(excelConnString))
                {
                    //Create OleDbCommand to fetch data from Excel 
                    using (OleDbCommand cmd = new OleDbCommand("Select * from [Sheet1$]", excelConnection))
                    {
                        excelConnection.Open();
                        using (OleDbDataReader dReader = cmd.ExecuteReader())
                        {
                            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnection))
                            {
                                //Give your Destination table name 
                                sqlBulk.DestinationTableName = "CA_SK_Auction";
                                //Column Mapping
                                sqlBulk.ColumnMappings.Add("경매일", "GAuctionDay");
                                sqlBulk.ColumnMappings.Add("출품번호", "GeventNo");
                                sqlBulk.ColumnMappings.Add("차량명", "GCarName");
                                sqlBulk.ColumnMappings.Add("차량번호", "CarNo");
                                sqlBulk.ColumnMappings.Add("시작가", "StartPrice");
                                sqlBulk.ColumnMappings.Add("낙찰가", "AuctionPrice");
                                sqlBulk.ColumnMappings.Add("낙찰결과", "AuctionState");
                                sqlBulk.ColumnMappings.Add("연식", "RegiYear");
                                sqlBulk.ColumnMappings.Add("주행거리", "Km");
                                sqlBulk.ColumnMappings.Add("미션", "AutoGbn");
                                sqlBulk.ColumnMappings.Add("연료", "Gas");
                                sqlBulk.ColumnMappings.Add("색상", "Color");
                                sqlBulk.ColumnMappings.Add("용도", "History");
                                sqlBulk.ColumnMappings.Add("성능점수", "EvalPt");
                                //End
                                sqlBulk.WriteToServer(dReader);
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }

        public int TransformExcel(string srcFilePath)
        {
            Excel.Application excelApp = null;
            Excel.Workbook inputWb = null;
            Excel.Worksheet inputWs = null;
            Excel.Workbook outputWb = null;
            Excel.Worksheet outputWs = null;
            int outputRowCount = 0;

            try
            {
                excelApp = new Excel.Application();
                inputWb = excelApp.Workbooks.Open(srcFilePath);
                inputWs = inputWb.Worksheets.get_Item(1) as Excel.Worksheet;

                //Template File Copy
                string tempFile = System.IO.Path.GetTempFileName();
                System.IO.File.Copy(TEMPLATE_FILEPATH, tempFile, true);

                outputWb = excelApp.Workbooks.Open(tempFile);
                outputWs = outputWb.Worksheets.get_Item(1) as Excel.Worksheet;

                Excel.Range rng = inputWs.UsedRange;
                object[,] data = rng.Value;

                int inputRowCount = data.GetLength(0);

                for (int r = 1; r <= inputRowCount; r++)
                {
                    if (data[r, 1] != null)
                    {
                        if (((Excel.Range)inputWs.Cells[r, 1]).Value2.ToString() == KEY_STRING)
                        {
                            dataReadStartPos = r;
                            outputRowCount = inputRowCount - dataReadStartPos;
                            break;
                        }
                    }
                }
                if (dataReadStartPos != 0 && outputRowCount != 0) //Start Position Found
                {
                    Excel.Range from = inputWs.Range["A" + (dataReadStartPos + 1) + ":F" + (dataReadStartPos + 1 + outputRowCount)] as Excel.Range;
                    Excel.Range to = outputWs.Range["C2:H" + (outputRowCount + 1)] as Excel.Range;
                    from.Copy(to);

                    from = inputWs.Range["G" + (dataReadStartPos + 1) + ":G" + (dataReadStartPos + 1 + outputRowCount)] as Excel.Range;
                    to = outputWs.Range["I2:I" + (outputRowCount + 1)] as Excel.Range;
                    from.Copy(to);


                    Excel.Range third = outputWs.Range["J2:J" + (outputRowCount + 1)];
                    third.Value2 = "=I2/10000";
                    Excel.Range fourth = outputWs.Range["J2:J" + (outputRowCount + 1)];
                    third.Copy(fourth);
                    fourth.Copy();
                    to.PasteSpecial(Excel.XlPasteType.xlPasteValues,
                                   Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone,
                                   System.Type.Missing, System.Type.Missing);
                    fourth.Delete();


                    from = inputWs.Range["H" + (dataReadStartPos + 1) + ":H" + (dataReadStartPos + 1 + outputRowCount)] as Excel.Range;
                    to = outputWs.Range["L2:L" + (outputRowCount + 1)] as Excel.Range;
                    from.Copy(to);

                    from = inputWs.Range["I" + (dataReadStartPos + 1) + ":I" + (dataReadStartPos + 1 + outputRowCount)] as Excel.Range;
                    to = outputWs.Range["M2:M" + (outputRowCount + 1)] as Excel.Range;
                    from.Copy(to);
                    to.Replace(What: "km", Replacement: "");

                    from = inputWs.Range["J" + (dataReadStartPos + 1) + ":S" + (dataReadStartPos + 1 + outputRowCount)] as Excel.Range;
                    to = outputWs.Range["N2:W" + (outputRowCount + 1)] as Excel.Range;
                    from.Copy(to);
                }
                outputWs.Columns.AutoFit();
                outputWb.Save();
                outputWb.Close(true);
                inputWb.Close(true);
                outputWb = null;
                inputWb = null;

                excelApp.Workbooks.Close();
                excelApp.Quit();

                System.IO.File.Copy(tempFile, TEST_FILEPATH, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ReleaseExcelObject(inputWs);
                ReleaseExcelObject(inputWb);
                ReleaseExcelObject(outputWs);
                ReleaseExcelObject(outputWb);
                ReleaseExcelObject(excelApp);
            }
            return outputRowCount;
        }
        static void ReleaseExcelObject(object obj)
        {
            try
            {
                if (obj != null)
                {
                    Marshal.ReleaseComObject(obj);
                    obj = null;
                }
            }
            catch (Exception ex)
            {
                obj = null;
                throw ex;
            }
            finally
            {
                GC.Collect();
            }
        }
    }

}
