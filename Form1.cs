﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace DBImporter
{
    public partial class DBImporterForm : Form
    {
        protected const string UPLOAD_SERVER_ADDR = "http://127.0.0.1:5000/";
        public DBImporterForm()
        {
            InitializeComponent();
        }

        private void ImportBtn_Click(object sender, EventArgs e)
        {
            DoTheJob();
        }
        
        private void setStatusTextSafe(string txt)
        {
            if (StatusText.InvokeRequired)
            { StatusText.Invoke(new Action(() => StatusText.Text = txt)); return; }
            StatusText.Text = txt;
        }

        public void DoTheJob()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            try
            {
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ExcelHandler eh = new ExcelHandler();
                    int transformedCnt = eh.TransformExcel(openFileDialog1.FileName);
                    if (transformedCnt > 0)
                    {
                        setStatusTextSafe("엑셀 파일 변환 완료");
                        if (eh.ImportExcelToDB(ExcelHandler.TEST_FILEPATH))
                        {
                            setStatusTextSafe(transformedCnt + "개의 행을 DB에 임포트 완료");
                            HTTPFileTransmitter hft = new HTTPFileTransmitter();
                            hft.Send(UPLOAD_SERVER_ADDR, ExcelHandler.TEST_FILEPATH);
                            setStatusTextSafe("변환된 엑셀 파일 전송 완료");
                            System.IO.File.Delete(ExcelHandler.TEST_FILEPATH);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
